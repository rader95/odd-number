package pl.sda;

public class OddNumberChecker {
    public static int check (int[] arr) {
        int number = arr[0];
        if (arr[0] != arr[1] && arr[1] == arr[2]) return number;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] != number) return arr[i];
        }
        return number;
    }
}
